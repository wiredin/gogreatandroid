package com.motiv.gogreat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;

public class SettingActivity extends ActionBarActivity {
	
	ImageView imgHeader;
	Button btnLogout;
	Typeface tf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		tf = Typeface.createFromAsset(getAssets(), "fonts/FARRAY.otf");
		
		imgHeader = (ImageView) findViewById(R.id.img_header);
//		AlphaAnimation alpha = new AlphaAnimation(.4F, .4F); // change values as you want
//		alpha.setDuration(0); // Make animation instant
//		alpha.setFillAfter(true); // Tell it to persist after the animation ends
//		// And then on your imageview
//		imgHeader.startAnimation(alpha);
		
		btnLogout = (Button) findViewById(R.id.btnLogout);
		btnLogout.setTypeface(tf);
		
	}
	
	

}
