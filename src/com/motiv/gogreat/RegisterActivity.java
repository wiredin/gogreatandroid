package com.motiv.gogreat;

import com.motiv.gogreat.services.RegisterService;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends ActionBarActivity {

	TextView skip, login;
	EditText uName, password;
	Typeface tf, tf_farray;
	Button btnReg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		tf = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		tf_farray = Typeface.createFromAsset(getAssets(), "fonts/FARRAY.otf");

		btnReg = (Button) findViewById(R.id.btn_reg);
		btnReg.setTypeface(tf_farray);

		uName = (EditText) findViewById(R.id.email);

		password = (EditText) findViewById(R.id.password);

		login = (TextView) findViewById(R.id.text_login);

		skip = (TextView) findViewById(R.id.text_skip);

		btnReg.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// RegisterActivity.this.startActivity(new Intent(
				// RegisterActivity.this, SettingActivity.class));
				RegisterService service = new RegisterService(RegisterActivity.this, uName
						.getText().toString(), password.getText().toString());
				service.execute();
			}
		});

		skip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RegisterActivity.this.startActivity(new Intent(
						RegisterActivity.this, MainActivity.class));
				finish();
			}
		});

		login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RegisterActivity.this.startActivity(new Intent(
						RegisterActivity.this, LoginActivity.class));
				finish();
			}
		});

	}
}
