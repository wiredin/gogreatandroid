package com.motiv.gogreat;

public class Itinerary {

	 public static final String TAG = Itinerary.class.getSimpleName();
	    
	    public String name;
	    public String year;
	    public String time;
	    
	    public Itinerary(String name, String year, String time) {
	            this.name = name;
	            this.year = year;
	            this.time = time;
	    }
	
}
