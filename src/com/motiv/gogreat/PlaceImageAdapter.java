package com.motiv.gogreat;

import com.motiv.gogreat.fragment.PlaceImageFragment;
import com.viewpagerindicator.IconPagerAdapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PlaceImageAdapter extends PagerAdapter implements
		IconPagerAdapter {
	
	Context context;
	String[] time;
    String[] country;
    String[] title;
    int[] images;
    
    LayoutInflater inflater;
	
	public PlaceImageAdapter(Context context, String[] title, String[] country,
            String[] time, int[] images) {
        this.context = context;
        this.time = time;
        this.country = country;
        this.title = title;
        this.images = images;
    }

	;

	protected static final int[] ICONS = new int[] { R.drawable.circle,
			R.drawable.circle, R.drawable.circle };

//	private int mCount = images.length;

	public PlaceImageAdapter(Fragment fragment) {
		super();
		// TODO Auto-generated constructor stub

	}
	
	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

	@Override
	public int getIconResId(int index) {
		// TODO Auto-generated method stub
		return ICONS[index % ICONS.length];
	}

	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return title.length;
	}

	@Override
    public Object instantiateItem(ViewGroup container, int position) {
 
        // Declare Variables
        TextView txtTitle;
        TextView txtcountry;
        TextView txtTime;
        ImageView imgHeader;
 
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.fragment_event_featured, container,
                false);
 
        // Locate the TextViews in viewpager_item.xml
//        txtTitle = (TextView) itemView.findViewById(R.id.title);
//        txtcountry = (TextView) itemView.findViewById(R.id.venue);
//        txtTime = (TextView) itemView.findViewById(R.id.time);
// 
//        // Capture position and set to the TextViews
//        txtTitle.setText(title[position]);
//        txtcountry.setText(country[position]);
//        txtTime.setText(time[position]);
 
        // Locate the ImageView in viewpager_item.xml
        imgHeader = (ImageView) itemView.findViewById(R.id.img_header);
        // Capture position and set to the ImageView
        imgHeader.setImageResource(images[position]);
//        AlphaAnimation alpha = new AlphaAnimation(.4F, .4F); // change values as you want
//		alpha.setDuration(0); // Make animation instant
//		alpha.setFillAfter(true); // Tell it to persist after the animation ends
//		// And then on your imageview
//		imgHeader.startAnimation(alpha);
 
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);
 
        return itemView;
    }
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }

}
