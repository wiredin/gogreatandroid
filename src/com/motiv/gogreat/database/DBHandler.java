package com.motiv.gogreat.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_NAME = "gogreat.db";

	// table name
	private static final String Table_Event = "event";
	private static final String Table_Attendees = "attendees";
	private static final String Table_Speakers = "speaker";
	private static final String Table_Profile = "profile";
	private static final String Table_Token = "token";
	private static final String Table_Buzz = "buzz";

	// Event
	private static final String Event_Table_Id = "_id";
	private static final String Event_Id = "event_id";
	private static final String Event_Day = "day";
	private static final String Event_Name = "name";
	private static final String Event_Slug = "slug";
	private static final String Event_Picture = "picture";
	private static final String Event_Organizer = "organizer";
	private static final String Event_Vanue = "vanue";
	private static final String Event_Time_Start = "time_start";
	private static final String Event_Time_End = "time_end";
	private static final String Event_Date = "date";
	private static final String Event_Text = "text";

	// Attendees
	private static final String Attend_Table_Id = "_id";
	private static final String Attend_Id = "attend_id";
	private static final String Attend_Name = "name";
	private static final String Attend_Picture = "picture";
	private static final String Attend_Thumbnail = "thumbnail";
	private static final String Attend_Company = "company";
	private static final String Attend_Events_Id = "events_id";

	// Speakers
	private static final String Speak_Table_Id = "_id";
	private static final String Speak_Id = "speak_id";
	private static final String Speak_Name = "name";
	private static final String Speak_Picture = "picture";
	private static final String Speak_Thumbnail = "thumbnail";
	private static final String Speak_Company = "company";
	private static final String Speak_Position = "position";
	private static final String Speak_Events = "events";
	private static final String Speak_Profile = "profile";

	// Profile
	private static final String Profile_Table_Id = "_id";
	private static final String Profile_User_Id = "user_id";
	private static final String Profile_First_Name = "first_name";
	private static final String Profile_Last_Name = "last_name";
	private static final String Profile_Email = "email";
	private static final String Profile_Password = "password";
	private static final String Profile_Phone_Number = "phone_number";
	private static final String Profile_Age = "age";
	private static final String Profile_Gender = "gender";
	private static final String Profile_Company_Name = "company_name";
	private static final String Profile_Position = "position";
	private static final String Profile_Company_Address = "company_address";
	private static final String Profile_Image = "profile_image";

	// Token
	private static final String Token_Id = "_id";
	private static final String Token_String = "token";
	private static final String Token_UserId = "userid";

	// Buzz
	private static final String Buzz_Table_Id = "_id";
	private static final String Buzz_Id = "buzz_id";
	private static final String Buzz_Time = "buzz_time";
	private static final String Buzz_Screen_Name = "buzz_screen_name";
	private static final String Buzz_Name = "buzz_name";
	private static final String Buzz_Picture = "buzz_picture";
	private static final String Buzz_Text = "buzz_text";

	public DBHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// create event
		String CREATE_TABLE_EVENT = "CREATE TABLE " + Table_Event + "("
				+ Event_Table_Id + " INTEGER PRIMARY KEY," + Event_Id
				+ " TEXT," + Event_Day + " TEXT," + Event_Name + " TEXT,"
				+ Event_Slug + " TEXT," + Event_Picture + " TEXT,"
				+ Event_Organizer + " TEXT," + Event_Vanue + " TEXT,"
				+ Event_Time_Start + " TEXT," + Event_Time_End + " TEXT,"
				+ Event_Date + " TEXT," + Event_Text + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_EVENT);

		// create attendees
		String CREATE_TABLE_ATTENDEES = "CREATE TABLE " + Table_Attendees + "("
				+ Attend_Table_Id + " INTEGER PRIMARY KEY," + Attend_Id
				+ " TEXT," + Attend_Name + " TEXT," + Attend_Picture + " TEXT,"
				+ Attend_Thumbnail + " TEXT," + Attend_Company + " TEXT,"
				+ Attend_Events_Id + " TEXT " + ")";
		db.execSQL(CREATE_TABLE_ATTENDEES);

		// create speakers
		String CREATE_TABLE_SPEAKERS = "CREATE TABLE " + Table_Speakers + "("
				+ Speak_Table_Id + " INTEGER PRIMARY KEY," + Speak_Id
				+ " TEXT," + Speak_Name + " TEXT," + Speak_Picture + " TEXT,"
				+ Speak_Thumbnail + " TEXT," + Speak_Company + " TEXT,"
				+ Speak_Position + " TEXT," + Speak_Events + " TEXT,"
				+ Speak_Profile + " TEXT " + ")";
		db.execSQL(CREATE_TABLE_SPEAKERS);

		// create profile
		String CREATE_TABLE_PROFILE = "CREATE TABLE " + Table_Profile + "("
				+ Profile_Table_Id + " INTEGER PRIMARY KEY," + Profile_User_Id
				+ " TEXT," + Profile_First_Name + " TEXT," + Profile_Last_Name
				+ " TEXT," + Profile_Email + " TEXT," + Profile_Password
				+ " TEXT," + Profile_Phone_Number + " TEXT," + Profile_Age
				+ " TEXT," + Profile_Gender + " TEXT," + Profile_Company_Name
				+ " TEXT," + Profile_Position + " TEXT,"
				+ Profile_Company_Address + " TEXT," + Profile_Image + " TEXT"
				+ ")";
		db.execSQL(CREATE_TABLE_PROFILE);

		// create token
		String CREATE_TABLE_TOKEN = "CREATE TABLE " + Table_Token + "("
				+ Token_Id + " INTEGER PRIMARY KEY," + Token_String + " TEXT,"
				+ Token_UserId + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_TOKEN);

		// create token
		String CREATE_TABLE_BUZZ = "CREATE TABLE " + Table_Buzz + "("
				+ Buzz_Table_Id + " INTEGER PRIMARY KEY," + Buzz_Id + " TEXT,"
				+ Buzz_Time + " TEXT," + Buzz_Screen_Name + " TEXT,"
				+ Buzz_Name + " TEXT," + Buzz_Picture + " TEXT," + Buzz_Text
				+ " TEXT" + ")";
		db.execSQL(CREATE_TABLE_BUZZ);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Table_Event);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Attendees);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Speakers);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Profile);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Token);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Buzz);
		onCreate(db);
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ########################### Function ######################
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ########################### Event ##########################

	// Add Event
	public void addEvent(Events event) {

		ContentValues values = new ContentValues();
		values.put(Event_Id, event.get_event_id());
		values.put(Event_Day, event.get_event_day());
		values.put(Event_Name, event.get_event_name());
		values.put(Event_Slug, event.get_event_slug());
		values.put(Event_Picture, event.get_event_picture());
		values.put(Event_Organizer, event.get_event_organizer());
		values.put(Event_Vanue, event.get_event_vanue());
		values.put(Event_Time_Start, event.get_event_time_start());
		values.put(Event_Time_End, event.get_event_time_end());
		values.put(Event_Date, event.get_event_date());
		values.put(Event_Text, event.get_event_text());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Event, null, values);
		db.close();

	}

	public List<Events> getAllEvent() {

		List<Events> eventList = new ArrayList<Events>();

		// Select All Query
		String query = "Select * " + "FROM " + Table_Event;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Events event = new Events();

				event.set_id(Integer.parseInt(cursor.getString(0)));
				event.set_event_id(cursor.getString(1));
				event.set_event_day(cursor.getString(2));
				event.set_event_name(cursor.getString(3));
				event.set_event_slug(cursor.getString(4));
				event.set_event_picture(cursor.getString(5));
				event.set_event_organizer(cursor.getString(6));
				event.set_event_vanue(cursor.getString(7));
				event.set_event_time_start(cursor.getString(8));
				event.set_event_time_end(cursor.getString(9));
				event.set_event_date(cursor.getString(10));
				event.set_event_text(cursor.getString(11));

				eventList.add(event);

			} while (cursor.moveToNext());
		} else {
			eventList = null;
		}
		db.close();

		// return contact list
		return eventList;
	}

	public List<Events> getEventByDay(String day) {

		List<Events> eventList = new ArrayList<Events>();

		// Select All Query
		String query = "Select * " + "FROM " + Table_Event + " WHERE "
				+ Event_Day + " =  \"" + day + "\"";
		;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Events event = new Events();

				event.set_id(Integer.parseInt(cursor.getString(0)));
				event.set_event_id(cursor.getString(1));
				event.set_event_day(cursor.getString(2));
				event.set_event_name(cursor.getString(3));
				event.set_event_slug(cursor.getString(4));
				event.set_event_picture(cursor.getString(5));
				event.set_event_organizer(cursor.getString(6));
				event.set_event_vanue(cursor.getString(7));
				event.set_event_time_start(cursor.getString(8));
				event.set_event_time_end(cursor.getString(9));
				event.set_event_date(cursor.getString(10));
				event.set_event_text(cursor.getString(11));

				eventList.add(event);

			} while (cursor.moveToNext());
		} else {
			eventList = null;
		}
		db.close();

		// return contact list
		return eventList;
	}

	public Events getEventById(String id) {

		// Select All Query
		String query = "Select * " + "FROM " + Table_Event + " WHERE "
				+ Event_Id + " =  \"" + id + "\"";
		;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Events event = new Events();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				event.set_id(Integer.parseInt(cursor.getString(0)));
				event.set_event_id(cursor.getString(1));
				event.set_event_day(cursor.getString(2));
				event.set_event_name(cursor.getString(3));
				event.set_event_slug(cursor.getString(4));
				event.set_event_picture(cursor.getString(5));
				event.set_event_organizer(cursor.getString(6));
				event.set_event_vanue(cursor.getString(7));
				event.set_event_time_start(cursor.getString(8));
				event.set_event_time_end(cursor.getString(9));
				event.set_event_date(cursor.getString(10));
				event.set_event_text(cursor.getString(11));

			} while (cursor.moveToNext());
		} else {
			event = null;
		}
		db.close();

		// return contact list
		return event;
	}

	public void deleteAllEvent() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Event);
		db.close();
	}

	// ########################### Speakers ##########################

	// Add Speakers
	public void addSpeakers(Speakers speak) {

		ContentValues values = new ContentValues();
		values.put(Speak_Id, speak.get_speak_id());
		values.put(Speak_Name, speak.get_speak_name());
		values.put(Speak_Picture, speak.get_speak_picture());
		values.put(Speak_Thumbnail, speak.get_speak_thumbnail());
		values.put(Speak_Company, speak.get_speak_company());
		values.put(Speak_Position, speak.get_speak_position());
		values.put(Speak_Events, speak.get_speak_events());
		values.put(Speak_Profile, speak.get_speak_profile());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Speakers, null, values);
		db.close();

	}

	public List<Speakers> getAllSpeaker() {

		List<Speakers> speakerList = new ArrayList<Speakers>();

		// Select All Query
		String query = "Select * " + "FROM " + Table_Speakers;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Speakers speaker = new Speakers();

				speaker.set_id(Integer.parseInt(cursor.getString(0)));
				speaker.set_speak_id(cursor.getString(1));
				speaker.set_speak_name(cursor.getString(2));
				speaker.set_speak_picture(cursor.getString(3));
				speaker.set_speak_thumbnail(cursor.getString(4));
				speaker.set_speak_company(cursor.getString(5));
				speaker.set_speak_position(cursor.getString(6));
				speaker.set_speak_events(cursor.getString(7));
				speaker.set_speak_profile(cursor.getString(8));

				speakerList.add(speaker);

			} while (cursor.moveToNext());
		} else {
			speakerList = null;
		}
		db.close();

		// return contact list
		return speakerList;
	}

	public void deleteAllSpeakers() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Speakers);
		db.close();
	}

	// ########################### Attendees ##########################

	// Add Attendees
	public void addAttendees(Attendees attend) {

		ContentValues values = new ContentValues();
		values.put(Attend_Id, attend.get_attend_id());
		values.put(Attend_Name, attend.get_attend_name());
		values.put(Attend_Picture, attend.get_attend_picture());
		values.put(Attend_Thumbnail, attend.get_attend_thumbnail());
		values.put(Attend_Company, attend.get_attend_company());

		String eventsId = null;
		for (Events event : attend.get_attend_events()) {
			if (eventsId == null) {
				eventsId = event.get_event_id();
			} else {
				eventsId = eventsId + " " + event.get_event_id();
			}
		}

		values.put(Attend_Events_Id, eventsId);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Attendees, null, values);
		db.close();

	}

	public void deleteAllAttendees() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Attendees);
		db.close();
	}

	// ########################### Token ##########################

	// Add Token
	public void addToken(String token, String userid) {

		ContentValues values = new ContentValues();
		values.put(Token_Id, 1);
		values.put(Token_String, token);
		values.put(Token_UserId, userid);

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Token, null, values);

		Log.d("Database", "Token inserted : " + token + " | " + userid);
		db.close();

	}

	// Update Token
	public void updateToken(String token) {

		ContentValues values = new ContentValues();
		values.put(Token_String, token);

		SQLiteDatabase db = this.getWritableDatabase();

		db.update(Table_Token, values, Token_Id + " = \"" + 1 + "\"", null);
		db.close();

	}

	// get Token
	public String getToken() {

		String query = "Select * " + "FROM " + Table_Token + " WHERE "
				+ Token_Id + " =  \"" + 1 + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String token = null;

		if (cursor.moveToFirst()) {
			do {
				token = cursor.getString(1);
			} while (cursor.moveToNext());
		}

		db.close();

		return token;
	}

	// get UserId
	public String getUserId() {

		String query = "Select * " + "FROM " + Table_Token + " WHERE "
				+ Token_Id + " =  \"" + 1 + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String token = null;

		if (cursor.moveToFirst()) {
			do {
				token = cursor.getString(2);
			} while (cursor.moveToNext());
		}

		db.close();

		return token;
	}

	public void deleteToken() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Token);
		db.close();
	}

	// ########################### Profile ##########################

	// Add Speakers
	public void addProfile(Profile profile) {

		ContentValues values = new ContentValues();
		values.put(Profile_Table_Id, 1);
		values.put(Profile_User_Id, profile.get_user_id());
		values.put(Profile_First_Name, profile.get_first_name());
		values.put(Profile_Last_Name, profile.get_last_name());
		values.put(Profile_Email, profile.get_email());
		values.put(Profile_Password, profile.get_password());
		values.put(Profile_Phone_Number, profile.get_phone_number());
		values.put(Profile_Age, profile.get_age());
		values.put(Profile_Gender, profile.get_gender());
		values.put(Profile_Company_Name, profile.get_company_name());
		values.put(Profile_Position, profile.get_position());
		values.put(Profile_Company_Address, profile.get_company_address());
		values.put(Profile_Image, profile.get_profile_image());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Profile, null, values);
		db.close();

	}

	public Profile getProfile() {

		// Select All Query
		String query = "Select * " + "FROM " + Table_Profile + " WHERE "
				+ Profile_Table_Id + " =  \"" + 1 + "\"";
		;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Profile profile = new Profile();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				profile.set_id(Integer.parseInt(cursor.getString(0)));
				profile.set_user_id(cursor.getString(1));
				profile.set_first_name(cursor.getString(2));
				profile.set_last_name(cursor.getString(3));
				profile.set_email(cursor.getString(4));
				profile.set_password(cursor.getString(5));
				profile.set_phone_number(cursor.getString(6));
				profile.set_age(cursor.getString(7));
				profile.set_gender(cursor.getString(8));
				profile.set_company_name(cursor.getString(9));
				profile.set_position(cursor.getString(10));
				profile.set_company_address(cursor.getString(11));
				profile.set_profile_image(cursor.getString(12));

			} while (cursor.moveToNext());
		} else {
			profile = null;
		}
		db.close();

		// return contact list
		return profile;
	}

	public void deleteProfile() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Profile);
		db.close();
	}

	// ########################### Buzz ##########################

	// Add Speakers
	public void addTweet(Buzz tweet) {

		ContentValues values = new ContentValues();
		values.put(Buzz_Id, tweet.get_buzz_id());
		values.put(Buzz_Time, tweet.get_buzz_time());
		values.put(Buzz_Screen_Name, tweet.get_buzz_screen());
		values.put(Buzz_Name, tweet.get_buzz_name());
		values.put(Buzz_Picture, tweet.get_buzz_picture());
		values.put(Buzz_Text, tweet.get_buzz_text());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(Table_Buzz, null, values);
		db.close();

	}

	public List<Buzz> getAllTweet() {

		List<Buzz> tweetList = new ArrayList<Buzz>();

		// Select All Query
		String query = "Select * " + "FROM " + Table_Buzz;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Buzz tweet = new Buzz();

				tweet.set_id(Integer.parseInt(cursor.getString(0)));
				tweet.set_buzz_id(cursor.getString(1));
				tweet.set_buzz_time(cursor.getString(2));
				tweet.set_buzz_screen(cursor.getString(3));
				tweet.set_buzz_name(cursor.getString(4));
				tweet.set_buzz_picture(cursor.getString(5));
				tweet.set_buzz_text(cursor.getString(6));

				tweetList.add(tweet);

			} while (cursor.moveToNext());
		} else {
			tweetList = null;
		}
		db.close();

		// return contact list
		return tweetList;
	}

	public void deleteAllTweet() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + Table_Buzz);
		db.close();
	}

}
