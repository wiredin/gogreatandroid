package com.motiv.gogreat.database;

public class Buzz {
	
	private int _id;
	private String buzz_id;
	private String buzz_time;
	private String buzz_screen_name;
	private String buzz_name;
	private String buzz_picture;
	private String buzz_text;

	//Setter
	public void set_id(int id) {
		this._id = id;
	}

	public void set_buzz_id(String buzz) {
		this.buzz_id = buzz;
	}
	
	public void set_buzz_time(String time) {
		this.buzz_time= time;
	}

	public void set_buzz_screen(String name) {
		this.buzz_screen_name = name;
	}
	
	public void set_buzz_name(String name) {
		this.buzz_name = name;
	}
	
	public void set_buzz_picture(String picture) {
		this.buzz_picture = picture;
	}
	
	public void set_buzz_text(String text) {
		this.buzz_text = text;
	}

	
	//Getter
	public int get_id() {
		return this._id;
	}

	public String get_buzz_id() {
		return this.buzz_id;
	}

	public String get_buzz_time() {
		return this.buzz_time;
	}
	
	public String get_buzz_name() {
		return this.buzz_name;
	}
	
	public String get_buzz_screen() {
		return this.buzz_screen_name;
	}
	
	public String get_buzz_picture() {
		return this.buzz_picture;
	}
	
	public String get_buzz_text() {
		return this.buzz_text;
	}

}
