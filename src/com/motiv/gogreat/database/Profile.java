package com.motiv.gogreat.database;

public class Profile {

	private int _id;
	private String user_id;
	private String first_name;
	private String last_name;
	private String email;
	private String password;
	private String phone_number;
	private String age;
	private String gender;
	private String company_name;
	private String position;
	private String company_address;
	private String profile_image;
	
	public Profile(){
		
	}

	public Profile(String user_id, String first_name, String last_name,
			String email, String phone_number, String age,
			String gender, String company_name, String position,
			String company_address, String profile_image) {
		this.user_id = user_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.phone_number = phone_number;
		this.age = age;
		this.gender = gender;
		this.company_name = company_name;
		this.position = position;
		this.company_address = company_address;
		this.profile_image = profile_image;
	}

	// Setter
	public void set_id(int id) {
		this._id = id;
	}

	public void set_user_id(String user_id) {
		this.user_id = user_id;
	}

	public void set_first_name(String first_name) {
		this.first_name = first_name;
	}

	public void set_last_name(String last_name) {
		this.last_name = last_name;
	}

	public void set_email(String email) {
		this.email = email;
	}

	public void set_password(String password) {
		this.password = password;
	}

	public void set_phone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public void set_age(String age) {
		this.age = age;
	}

	public void set_gender(String gender) {
		this.gender = gender;
	}

	public void set_company_name(String company_name) {
		this.company_name = company_name;
	}

	public void set_position(String position) {
		this.position = position;
	}

	public void set_company_address(String company_address) {
		this.company_address = company_address;
	}
	
	public void set_profile_image(String profile_image) {
		this.profile_image = profile_image;
	}

	// Getter
	public int get_id() {
		return this._id;
	}

	public String get_user_id() {
		return this.user_id;
	}

	public String get_first_name() {
		return this.first_name;
	}

	public String get_last_name() {
		return this.last_name;
	}

	public String get_email() {
		return this.email;
	}

	public String get_password() {
		return this.password;
	}

	public String get_phone_number() {
		return this.phone_number;
	}

	public String get_age() {
		return this.age;
	}

	public String get_gender() {
		return this.gender;
	}

	public String get_company_name() {
		return this.company_name;
	}

	public String get_position() {
		return this.position;
	}

	public String get_company_address() {
		return this.company_address;
	}
	
	public String get_profile_image() {
		return this.profile_image;
	}
}
