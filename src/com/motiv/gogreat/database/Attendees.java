package com.motiv.gogreat.database;

import java.util.ArrayList;
import java.util.List;

public class Attendees {
	
	private int _id;
	private String attend_id;
	private String attend_name;
	private String attend_picture;
	private String attend_thumbnail;
	private String attend_company;
	private List<Events> attend_events;
	
	public Attendees(){
		this.attend_events = new ArrayList<Events>();
	}
	
	//Setter
	public void set_id(int id) {
		this._id = id;
	}

	public void set_attend_id(String attend) {
		this.attend_id = attend;
	}

	public void set_attend_name(String name) {
		this.attend_name = name;
	}

	public void set_attend_picture(String picture) {
		this.attend_picture = picture;
	}

	public void set_attend_thumbnail(String thumbnail) {
		this.attend_thumbnail = thumbnail;
	}

	public void set_attend_company(String vanue) {
		this.attend_company = vanue;
	}

	public void set_attend_events(List<Events> eventList) {
		this.attend_events = eventList;
	}
	
	//Getter
	public int get_id() {
		return this._id;
	}

	public String get_attend_id() {
		return this.attend_id;
	}

	public String get_attend_name() {
		return this.attend_name;
	}

	public String get_attend_picture() {
		return this.attend_picture;
	}

	public String get_attend_thumbnail() {
		return this.attend_thumbnail;
	}

	public String get_attend_company() {
		return this.attend_company;
	}

	public List<Events> get_attend_events() {
		return this.attend_events;
	}
	
}
