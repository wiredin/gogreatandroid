package com.motiv.gogreat.database;

public class Speakers {
	
	private int _id;
	private String speak_id;
	private String speak_name;
	private String speak_picture;
	private String speak_thumbnail;
	private String speak_company;
	private String speak_position;
	private String speak_events;
	private String speak_profile;
	
	//Setter
	public void set_id(int id) {
		this._id = id;
	}

	public void set_speak_id(String speak) {
		this.speak_id = speak;
	}

	public void set_speak_name(String name) {
		this.speak_name = name;
	}

	public void set_speak_picture(String picture) {
		this.speak_picture = picture;
	}

	public void set_speak_thumbnail(String thumbnail) {
		this.speak_thumbnail = thumbnail;
	}

	public void set_speak_company(String vanue) {
		this.speak_company = vanue;
	}

	public void set_speak_position(String position) {
		this.speak_position = position;
	}
	
	public void set_speak_events(String event) {
		this.speak_events = event;
	}
	
	public void set_speak_profile(String profile) {
		this.speak_profile = profile;
	}
	
	//Getter
	public int get_id() {
		return this._id;
	}

	public String get_speak_id() {
		return this.speak_id;
	}

	public String get_speak_name() {
		return this.speak_name;
	}

	public String get_speak_picture() {
		return this.speak_picture;
	}

	public String get_speak_thumbnail() {
		return this.speak_thumbnail;
	}

	public String get_speak_company() {
		return this.speak_company;
	}

	public String get_speak_position() {
		return this.speak_position;
	}
	
	public String get_speak_events() {
		return this.speak_events;
	}
	
	public String get_speak_profile() {
		return this.speak_profile;
	}
}
