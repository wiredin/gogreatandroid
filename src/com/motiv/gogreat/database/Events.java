package com.motiv.gogreat.database;

public class Events {
	
	private int _id;
	private String event_id;
	private String event_day;
	private String event_name;
	private String event_slug;
	private String event_picture;
	private String event_organizer;
	private String event_vanue;
	private String event_time_start;
	private String event_time_end;
	private String event_date;
	private String event_text;

	//Setter
	public void set_id(int id) {
		this._id = id;
	}

	public void set_event_id(String event) {
		this.event_id = event;
	}
	
	public void set_event_day(String day) {
		this.event_day= day;
	}

	public void set_event_name(String name) {
		this.event_name = name;
	}
	
	public void set_event_slug(String slug) {
		this.event_slug = slug;
	}

	public void set_event_picture(String picture) {
		this.event_picture = picture;
	}

	public void set_event_organizer(String organizer) {
		this.event_organizer = organizer;
	}

	public void set_event_vanue(String vanue) {
		this.event_vanue = vanue;
	}

	public void set_event_time_start(String time) {
		this.event_time_start = time;
	}
	
	public void set_event_time_end(String time) {
		this.event_time_end = time;
	}
	
	public void set_event_date(String date) {
		this.event_date = date;
	}
	
	public void set_event_text(String text) {
		this.event_text = text;
	}
	
	//Getter
	public int get_id() {
		return this._id;
	}

	public String get_event_id() {
		return this.event_id;
	}

	public String get_event_day() {
		return this.event_day;
	}
	
	public String get_event_name() {
		return this.event_name;
	}
	
	public String get_event_slug() {
		return this.event_slug;
	}

	public String get_event_picture() {
		return this.event_picture;
	}

	public String get_event_organizer() {
		return this.event_organizer;
	}

	public String get_event_vanue() {
		return this.event_vanue;
	}

	public String get_event_time_start() {
		return this.event_time_start;
	}
	
	public String get_event_time_end() {
		return this.event_time_end;
	}
	
	public String get_event_date() {
		return this.event_date;
	}
	
	public String get_event_text() {
		return this.event_text;
	}

}
