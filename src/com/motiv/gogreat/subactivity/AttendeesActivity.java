package com.motiv.gogreat.subactivity;

import com.motiv.gogreat.R;
import com.motiv.gogreat.fragment.ItineraryFragment;
import com.motiv.gogreat.fragment.ListEventFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

public class AttendeesActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendees);

		Fragment newFragment = new ItineraryFragment();
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		// Replace whatever is in the fragment_container view with
		// this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.itinerary_container, newFragment);

		// Commit the transaction
		transaction.commit();

	}

}
