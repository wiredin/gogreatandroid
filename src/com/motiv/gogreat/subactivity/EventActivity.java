package com.motiv.gogreat.subactivity;

import com.motiv.gogreat.R;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;
import com.motiv.gogreat.fragment.EventAttendessFragment;
import com.motiv.gogreat.fragment.EventDiscFragment;
import com.motiv.gogreat.fragment.EventSpeakerFragment;
import com.motiv.gogreat.fragment.ListEventFragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EventActivity extends ActionBarActivity {

	Button btnAttent;
	RelativeLayout tab1, tab2, tab3;
	TextView txtTab1, txtTab2, txtTab3;
	View bar1, bar2, bar3;

	Typeface tf;
	
	DBHandler dbHandler;
	String eventid = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		
		dbHandler = new DBHandler(this, null, null, 0);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    eventid = extras.getString("EventId");
		}
		
		Events event = dbHandler.getEventById(eventid);

		tf = Typeface.createFromAsset(getAssets(), "fonts/FARRAY.otf");

		bar1 = (View) findViewById(R.id.view_tab1);
//		bar2 = (View) findViewById(R.id.view_tab2);
//		bar3 = (View) findViewById(R.id.view_tab3);

		txtTab1 = (TextView) findViewById(R.id.txt_tab1);
//		txtTab2 = (TextView) findViewById(R.id.txt_tab2);
//		txtTab3 = (TextView) findViewById(R.id.txt_tab3);

		btnAttent = (Button) findViewById(R.id.btn_attent);
		btnAttent.setTypeface(tf);
		btnAttent.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnAttent.setTextColor(getResources().getColor(R.color.white));
			}
		});

		tab1 = (RelativeLayout) findViewById(R.id.tab_details);
		tab1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bar1.setVisibility(View.VISIBLE);
//				bar2.setVisibility(View.INVISIBLE);
//				bar3.setVisibility(View.INVISIBLE);

				txtTab1.setTextColor(getResources().getColor(R.color.theme));
//				txtTab2.setTextColor(getResources().getColor(R.color.black));
//				txtTab3.setTextColor(getResources().getColor(R.color.black));

				if (getSupportFragmentManager().findFragmentById(
						R.id.event_content) != null) {

					Fragment newFragment = new EventDiscFragment();
					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.event_content, newFragment);

					// Commit the transaction
					transaction.commit();
				}

			}
		});
//		tab2 = (RelativeLayout) findViewById(R.id.tab_speakers);
//		tab2.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				bar1.setVisibility(View.INVISIBLE);
//				bar2.setVisibility(View.VISIBLE);
//				bar3.setVisibility(View.INVISIBLE);
//
//				txtTab1.setTextColor(getResources().getColor(R.color.black));
//				txtTab2.setTextColor(getResources().getColor(R.color.theme));
//				txtTab3.setTextColor(getResources().getColor(R.color.black));
//
//				if (getSupportFragmentManager().findFragmentById(
//						R.id.event_content) != null) {
//
//					Fragment newFragment = new EventSpeakerFragment();
//					FragmentTransaction transaction = getSupportFragmentManager()
//							.beginTransaction();
//
//					// Replace whatever is in the fragment_container view with
//					// this fragment,
//					// and add the transaction to the back stack
//					transaction.replace(R.id.event_content, newFragment);
//
//					// Commit the transaction
//					transaction.commit();
//				}
//			}
//		});
//		tab3 = (RelativeLayout) findViewById(R.id.tab_desc);
//		tab3.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				bar1.setVisibility(View.INVISIBLE);
//				bar2.setVisibility(View.INVISIBLE);
//				bar3.setVisibility(View.VISIBLE);
//
//				txtTab1.setTextColor(getResources().getColor(R.color.black));
//				txtTab2.setTextColor(getResources().getColor(R.color.black));
//				txtTab3.setTextColor(getResources().getColor(R.color.theme));
//
//				if (getSupportFragmentManager().findFragmentById(
//						R.id.event_content) != null) {
//
//					Fragment newFragment = new EventAttendessFragment();
//					FragmentTransaction transaction = getSupportFragmentManager()
//							.beginTransaction();
//
//					// Replace whatever is in the fragment_container view with
//					// this fragment,
//					// and add the transaction to the back stack
//					transaction.replace(R.id.event_content, newFragment);
//
//					// Commit the transaction
//					transaction.commit();
//				}
//			}
//		});
	}

	@Override
	public void onResume() {
		super.onResume();

		if (getSupportFragmentManager().findFragmentById(R.id.event_content) == null) {
			
			Bundle bundle = new Bundle();
			bundle.putString("EventId", eventid);
			
			Fragment newFragment = new EventDiscFragment();
			newFragment.setArguments(bundle);
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();

			// Replace whatever is in the fragment_container view with
			// this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.event_content, newFragment);

			// Commit the transaction
			transaction.commit();
		}
	}
}
