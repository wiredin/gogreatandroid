package com.motiv.gogreat;


import com.motiv.gogreat.fragment.BuzzFragment;
import com.motiv.gogreat.fragment.EventFragment;
import com.motiv.gogreat.fragment.ItineraryFragment;
import com.motiv.gogreat.fragment.PeopleFragment;
import com.motiv.gogreat.fragment.SettingFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
	
	ViewPager mPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);

		PagerAdapter adapter = new FragmentStatePagerAdapter(
				getSupportFragmentManager()) {
			@Override
			public Fragment getItem(int position) {
				switch (position) {
				case 0:
					return new EventFragment();
				case 1:
					return new ItineraryFragment();
				case 2:
					return new PeopleFragment();
				case 3:
					return new BuzzFragment();
				case 4:
					return new SettingFragment();
//				case 4:
//					return new GalleryFragment();
				}
				return null;
			}

			@Override
			public int getCount() {
				return 5;
			}

		};
		
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				getSupportActionBar().setSelectedNavigationItem(position);
				setTitle(getTitleFromPosition(position));
			}
		});

		mPager.setPageMargin(getResources().getDimensionPixelSize(
				R.dimen.page_margin));

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int position = 0; position < adapter.getCount(); position++) {
			
			actionBar.addTab(actionBar.newTab().setIcon(this.getPageIcon(position)).setTabListener(this));
		}
		
		Bundle bun = getIntent().getExtras();
		if (bun != null){
			mPager.setCurrentItem(3);
		}
		 
	}
	
	public Drawable getPageIcon(int position) {
		switch (position) {
		case 0:
			return getResources().getDrawable(R.drawable.event_icon);
		case 1:
			return getResources().getDrawable(R.drawable.itinerary_icon);
		case 2:
			return getResources().getDrawable(R.drawable.people_icon);
		case 3:
			return getResources().getDrawable(R.drawable.buzz_icon);
		case 4:
			return getResources().getDrawable(R.drawable.ic_action_settings);
//		case 4:
//			return getResources().getDrawable(R.drawable.gallery_icons);
		}
		return null;
	}
	
	public String getTitleFromPosition(int position){
		String title;
		switch (position) {
		case 0:
			title="Event";
			return title;
		case 1:
			title="Itinerary";
			return title;
		case 2:
			title="People";
			return title;
		case 3:
			title="Buzz";
			return title;
		case 4:
			title="Setting";
			return title;
//		case 4:
//			title="Gallery";
//			return title;
		}
		return null;
	}
	

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			Intent myIntent = new Intent(this,SettingActivity.class);
//	        startActivity(myIntent);
//	        
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mPager.setCurrentItem(tab.getPosition());
		Log.d("fared", "position " +tab.getPosition());
		switch (tab.getPosition()) {
		case 0:
			tab.setIcon(getResources().getDrawable(R.drawable.event_icon_2));
			break;
		case 1:
			tab.setIcon(getResources().getDrawable(R.drawable.itinerary_icon_2));
			break;
		case 2:
			tab.setIcon(getResources().getDrawable(R.drawable.people_icon_2));
			break;
		case 3:
			tab.setIcon(getResources().getDrawable(R.drawable.buzz_icon_2));
			break;
		case 4:
			tab.setIcon(getResources().getDrawable(R.drawable.ic_action_settings_2));
			break;
//		case 4:
//			tab.setIcon(getResources().getDrawable(R.drawable.gallery_icons_2));
//			break;
		}
	}


	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		switch (tab.getPosition()) {
		case 0:
			tab.setIcon(getResources().getDrawable(R.drawable.event_icon));
			break;
		case 1:
			tab.setIcon(getResources().getDrawable(R.drawable.itinerary_icon));
			break;
		case 2:
			tab.setIcon(getResources().getDrawable(R.drawable.people_icon));
			break;
		case 3:
			tab.setIcon(getResources().getDrawable(R.drawable.buzz_icon));
			break;
		case 4:
			tab.setIcon(getResources().getDrawable(R.drawable.ic_action_settings));
			break;
//		case 4:
//			tab.setIcon(getResources().getDrawable(R.drawable.gallery_icons));
//			break;
		}
	}
	
	


	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
}
