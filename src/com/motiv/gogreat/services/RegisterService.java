package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.motiv.gogreat.LoginActivity;
import com.motiv.gogreat.MainActivity;
import com.motiv.gogreat.RegisterActivity;
import com.motiv.gogreat.SettingActivity;
import com.motiv.gogreat.database.DBHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class RegisterService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/endpoint/register-app.php";

	Context context;
	String email;
	String password;

	ProgressDialog progressDialog;

	public RegisterService(Context context, String email, String password) {
		this.context = context;
		this.email = email;
		this.password = password;
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("email", this.email));
			nameValuePairs
					.add(new BasicNameValuePair("password", this.password));
			nameValuePairs.add(new BasicNameValuePair("re_password",
					this.password));
			nameValuePairs.add(new BasicNameValuePair("registration_source",
					"android"));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		DBHandler dbHandler = new DBHandler(context, null, null, 0);

		if (result != null) {
			progressDialog.dismiss();

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				// JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);

				String error = objects.getString("error");

				if (error.equals("false")) {
					Log.d("Register Service Object", "done");

					dbHandler.deleteProfile();
					dbHandler.deleteToken();
					dbHandler.deleteAllSpeakers();

					// proceed if no problem
					LoginService service = new LoginService(context, email,
							password,true);

					service.execute();

				} else {
					Log.d("Register Service Object", "Error");
				}

			} catch (Exception e) {
				Log.e("Register Service Object", "fail");
			}
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

}
