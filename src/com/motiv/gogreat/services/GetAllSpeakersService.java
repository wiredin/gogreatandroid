package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.motiv.gogreat.MainActivity;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;
import com.motiv.gogreat.database.Speakers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetAllSpeakersService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/endpoint/get-people.php";

	Context context;
	boolean register;

	ProgressDialog progressDialog;
	DBHandler dbHandler;

	public GetAllSpeakersService(Context context, boolean register) {
		this.context = context;
		this.register = register;

		dbHandler = new DBHandler(context, null, null, 0);
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("user_ID", dbHandler
					.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("token", dbHandler
					.getToken()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		Log.d("Get Speakers", "userid: " + dbHandler.getUserId() + " token: "
				+ dbHandler.getToken());

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading All People...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null) {
			progressDialog.dismiss();

			dbHandler = new DBHandler(context, null, null, 0);

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				// JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);

				String error = objects.getString("error");

				if (error.equals("false")) {
					Log.d("People Service Object", "done");

					// Save Profile
					JSONArray peoples = objects.getJSONArray("people");

					dbHandler.deleteAllSpeakers();

					JSONObject[] people = new JSONObject[peoples.length()];
					for (int i = 0; i < peoples.length(); i++) {
						people[i] = peoples.getJSONObject(i);

						Speakers speak = new Speakers();
						
						Log.d("People Service Object", "People "+i);

						speak.set_speak_name(people[i].getString("first_name")
								+ " " + people[i].getString("last_name"));
						speak.set_speak_company(people[i].getString("company"));
						speak.set_speak_position(people[i]
								.getString("position"));
						speak.set_speak_profile(people[i].getString("gender"));

						dbHandler.addSpeakers(speak);

					}

					// proceed if no problem
					Intent myIntent = new Intent(context, MainActivity.class);
					
					if (this.register == true){
						myIntent.putExtra("Register", "true");
					}
					context.startActivity(myIntent);
					((Activity) context).finish();

				} else {
					Log.e("People Service", "Error - " + data);
				}

			} catch (Exception e) {
				Log.e("People Service Object", "fail -" + e);
			}
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

}
