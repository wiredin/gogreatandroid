package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.motiv.gogreat.MainActivity;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetAllEventService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/api/get-all-events";

	Context context;

	ProgressDialog progressDialog;
	DBHandler dbHandler;

	public GetAllEventService(Context context) {
		this.context = context;
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading All Event...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null) {
			progressDialog.dismiss();
			
			dbHandler = new DBHandler(context, null, null, 0);

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				// JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);
				
				JSONArray day1 = objects.getJSONArray("20140917");
				JSONArray day2 = objects.getJSONArray("20140918");
				JSONArray day3 = objects.getJSONArray("20140919");
				JSONArray day4 = objects.getJSONArray("20140920");
				
				Log.d("Event Service Object", "done days");
				
				JSONObject[] eventraw1 = new JSONObject[day1.length()];
				
				for (int i = 0; i < day1.length(); i++) {
					eventraw1[i] = day1.getJSONObject(i);
					
					Events event = new Events();
					
					Log.d("Event Service Object", "day 1 - "+i);
					
					event.set_event_id(eventraw1[i].getString("ID"));
					event.set_event_day("1");
					event.set_event_name(eventraw1[i].getString("event_name"));
					event.set_event_slug(eventraw1[i].getString("event_slug"));
					event.set_event_picture(eventraw1[i].getString("event_image"));
					event.set_event_organizer(eventraw1[i].getString("organizer"));
					event.set_event_vanue(eventraw1[i].getString("venue"));
					event.set_event_time_start(eventraw1[i].getString("time_start"));
					event.set_event_time_end(eventraw1[i].getString("time_end"));
					event.set_event_date(eventraw1[i].getString("date"));
					event.set_event_text(eventraw1[i].getString("event_description"));
					
					dbHandler.addEvent(event);
				}
				
				Log.d("Event Service Object", "done 1");
				
				JSONObject[] eventraw2 = new JSONObject[day2.length()];
				for (int i = 0; i < day2.length(); i++) {
					eventraw2[i] = day2.getJSONObject(i);
					
					Events event = new Events();
					
					Log.d("Event Service Object", "day 2 - "+i);
					
					event.set_event_id(eventraw2[i].getString("ID"));
					event.set_event_day("2");
					event.set_event_name(eventraw2[i].getString("event_name"));
					event.set_event_slug(eventraw2[i].getString("event_slug"));
					event.set_event_picture(eventraw2[i].getString("event_image"));
					event.set_event_organizer(eventraw2[i].getString("organizer"));
					event.set_event_vanue(eventraw2[i].getString("venue"));
					event.set_event_time_start(eventraw2[i].getString("time_start"));
					event.set_event_time_end(eventraw2[i].getString("time_end"));
					event.set_event_date(eventraw2[i].getString("date"));
					event.set_event_text(eventraw2[i].getString("event_description"));
					
					dbHandler.addEvent(event);
				}
				
				Log.d("Event Service Object", "done 2");
				
				JSONObject[] eventraw3 = new JSONObject[day3.length()];
				for (int i = 0; i < day3.length(); i++) {
					eventraw3[i] = day3.getJSONObject(i);
					
					Events event = new Events();
					
					Log.d("Event Service Object", "day 3 - "+i);
					
					event.set_event_id(eventraw3[i].getString("ID"));
					event.set_event_day("3");
					event.set_event_name(eventraw3[i].getString("event_name"));
					event.set_event_slug(eventraw3[i].getString("event_slug"));
					event.set_event_picture(eventraw3[i].getString("event_image"));
					event.set_event_organizer(eventraw3[i].getString("organizer"));
					event.set_event_vanue(eventraw3[i].getString("venue"));
					event.set_event_time_start(eventraw3[i].getString("time_start"));
					event.set_event_time_end(eventraw3[i].getString("time_end"));
					event.set_event_date(eventraw3[i].getString("date"));
					event.set_event_text(eventraw3[i].getString("event_description"));
					
					dbHandler.addEvent(event);
				}
				
				Log.d("Event Service Object", "done 3");
				
				JSONObject[] eventraw4 = new JSONObject[day4.length()];
				for (int i = 0; i < day4.length(); i++) {
					eventraw4[i] = day4.getJSONObject(i);
					
					Events event = new Events();
					
					Log.d("Event Service Object", "day 4 - "+i);
					
					event.set_event_id(eventraw4[i].getString("ID"));
					event.set_event_day("4");
					event.set_event_name(eventraw4[i].getString("event_name"));
					event.set_event_slug(eventraw4[i].getString("event_slug"));
					event.set_event_picture(eventraw4[i].getString("event_image"));
					event.set_event_organizer(eventraw4[i].getString("organizer"));
					event.set_event_vanue(eventraw4[i].getString("venue"));
					event.set_event_time_start(eventraw4[i].getString("time_start"));
					event.set_event_time_end(eventraw4[i].getString("time_end"));
					event.set_event_date(eventraw4[i].getString("date"));
					event.set_event_text(eventraw4[i].getString("event_description"));
					
					dbHandler.addEvent(event);
				}
				
				Log.d("Event Service Object", "done All");
				

			} catch (Exception e) {
				Log.e("Event Service Object", "fail -"+e);
			}
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

}
