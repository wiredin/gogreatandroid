package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateProfileService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/endpoint/edit_profile_app.php";

	Context context;
	String firstname, lastname, email, compname, compaddr, position,
			age, gender, compurl, phone;

	ProgressDialog progressDialog;

	public UpdateProfileService(Context context, Profile profile) {
		this.context = context;
		this.email = profile.get_email();
		this.firstname = profile.get_first_name();
		this.lastname = profile.get_last_name();
		this.email = profile.get_email();
		this.compname = profile.get_company_name();
		this.compaddr = profile.get_company_address();
		this.position = profile.get_position();
		this.age = profile.get_age();
		this.gender = profile.get_gender();
		this.compurl = null;
		this.phone = profile.get_phone_number();
	}

	@Override
	protected String doInBackground(Void... params) {
		
		DBHandler dbHandler = new DBHandler(context, null, null, 0);

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("user_ID", dbHandler.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("first_name",
					this.firstname));
			nameValuePairs.add(new BasicNameValuePair("last_name",
					this.lastname));
			nameValuePairs.add(new BasicNameValuePair("email", this.email));
			nameValuePairs
					.add(new BasicNameValuePair("company", this.compname));
			nameValuePairs.add(new BasicNameValuePair("company_address",
					this.compaddr));
			nameValuePairs.add(new BasicNameValuePair("phone_number",
					this.phone));
			nameValuePairs
					.add(new BasicNameValuePair("position", this.position));
			nameValuePairs.add(new BasicNameValuePair("company_url",
					this.compurl));
			nameValuePairs.add(new BasicNameValuePair("age", this.age));
			nameValuePairs.add(new BasicNameValuePair("gender", this.gender));
			nameValuePairs.add(new BasicNameValuePair("token", dbHandler.getToken()));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null) {
			progressDialog.dismiss();

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				// JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);

				String error = objects.getString("error");

				if (error.equals("false")) {
					Log.d("Update Service Object", "done");

					// proceed if no problem
					showText("Update Done!");

				} else {
					Log.e("Update Service Object", "Error - "+data);
					showText("Update Error!");
				}

			} catch (Exception e) {
				Log.e("Update Service Object", "fail");
			}
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	public void showText(String text) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("GREAT");

		// set dialog message
		alertDialogBuilder.setMessage(text).setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
