package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.motiv.gogreat.database.Buzz;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.fragment.BuzzFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class GetBuzzService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/endpoint/twitter.php";

	Context context;
	BuzzFragment buzz;

	ProgressDialog progressDialog;
	DBHandler dbHandler;

	public GetBuzzService(Context context, BuzzFragment buzz) {
		this.context = context;
		this.buzz = buzz;

		dbHandler = new DBHandler(context, null, null, 0);
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("user_ID", dbHandler
					.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("token", dbHandler
					.getToken()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading Tweets...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null) {
			progressDialog.dismiss();

			dbHandler = new DBHandler(context, null, null, 0);

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				// JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);

				String error = objects.getString("error");

				if (error.equals("false")) {
					Log.d("Tweet Service Object", "done");

					// Save Profile
					JSONArray tweets = objects.getJSONArray("tweets");

					dbHandler.deleteAllTweet();

					JSONObject[] tweetsObj = new JSONObject[tweets.length()];
					for (int i = 0; i < tweets.length(); i++) {
						tweetsObj[i] = tweets.getJSONObject(i);

						Buzz tweet = new Buzz();

						Log.d("Tweet Service Object", "Tweet " + i);

						tweet.set_buzz_time(tweetsObj[i].getString("time"));
						tweet.set_buzz_text(tweetsObj[i].getString("text"));
						tweet.set_buzz_screen("@"
								+ tweetsObj[i].getString("screen_name"));
						tweet.set_buzz_name(tweetsObj[i].getString("name"));
						tweet.set_buzz_picture(tweetsObj[i]
								.getString("profile_image_url"));

						dbHandler.addTweet(tweet);

						buzz.loadTweet();

					}

				} else {
					Log.e("Tweet Service", "Error - " + data);
				}

			} catch (Exception e) {
				Log.e("Tweet Service Object", "fail -" + e);
			}
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

}
