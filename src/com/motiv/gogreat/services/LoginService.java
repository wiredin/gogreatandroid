package com.motiv.gogreat.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.motiv.gogreat.MainActivity;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class LoginService extends AsyncTask<Void, Void, String> {

	private static final String http = "http://gogreat.my/endpoint/login-app.php";

	Context context;
	String email;
	String password;
	boolean register = false;

	ProgressDialog progressDialog;
	DBHandler dbHandler;

	public LoginService(Context context, String email, String password) {
		this.context = context;
		this.email = email;
		this.password = password;
	}
	
	public LoginService(Context context, String email, String password, boolean register) {
		this.context = context;
		this.email = email;
		this.password = password;
		this.register = register;
	}

	@Override
	protected String doInBackground(Void... params) {

		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(http);
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("email", this.email));
			nameValuePairs
					.add(new BasicNameValuePair("password", this.password));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		Log.d("Login Service", "Email : "+email+" | Password : "+password);

		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("GoGreat");
		progressDialog.setMessage("Loading...");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null) {
			progressDialog.dismiss();

			dbHandler = new DBHandler(context, null, null, 0);

			// process result
			JSONObject objects = null;
			try {
				String data = result;
				//JSONArray array = new JSONArray(data);
				objects = new JSONObject(data);
				
				String error = objects.getString("error");
				
				if (error.equals("false")){
					Log.d("Login Service Object","done");
					
					//Save Profile 
					JSONObject user = objects.getJSONObject("user");
					
					Profile profile = new Profile();
					
					profile.set_user_id(user.getString("ID"));
					profile.set_first_name(user.getString("first_name"));
					profile.set_last_name(user.getString("last_name"));
					profile.set_email(user.getString("email"));
					profile.set_phone_number(user.getString("phone_number"));
					profile.set_age(user.getString("age"));
					profile.set_gender(user.getString("gender"));
					profile.set_company_name(user.getString("company"));
					profile.set_company_address(user.getString("company_address"));
					profile.set_position(user.getString("position"));
					profile.set_profile_image(user.getString("profile_img"));
					
					dbHandler.deleteProfile();
					dbHandler.addProfile(profile);
					
					//Save Token for future use
					dbHandler.deleteToken();
					dbHandler.addToken(objects.getString("current_token"),user.getString("ID"));
					
					GetAllSpeakersService service = new GetAllSpeakersService(context,register);
					service.execute();
					
				} else {
					Log.e("Login Service","Error");
				}

			} catch (Exception e) {
				Log.e("Login Service Object","fail");
			}
			
			Log.d("Login Service Text", result);
		} else {
			Log.e("Login Service ", "Result Empty - Server Error");
		}
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

}
