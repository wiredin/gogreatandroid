package com.motiv.gogreat;

import com.motiv.gogreat.database.DBHandler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class SplashScreenActivity extends ActionBarActivity {
	
	DBHandler dbHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		
		dbHandler = new DBHandler(this, null, null, 0);

		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					try {
						if (dbHandler.getToken().isEmpty()){
							SplashScreenActivity.this.startActivity(new Intent(
									SplashScreenActivity.this, LoginActivity.class));
						} else {
							SplashScreenActivity.this.startActivity(new Intent(
									SplashScreenActivity.this, MainActivity.class));
						}
					} catch (Exception e) {
						SplashScreenActivity.this.startActivity(new Intent(
								SplashScreenActivity.this, LoginActivity.class));
					}
					
					finish();

				}
			}
		};
		timer.start();
	}

}
