package com.motiv.gogreat.fragment;

import java.util.List;

import com.motiv.gogreat.R;
import com.motiv.gogreat.RoundedTransformation;
import com.motiv.gogreat.SettingActivity;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;
import com.motiv.gogreat.subactivity.EventActivity;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListEventFragment extends Fragment {

	DBHandler dbHandler;
	String[] event_id;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_list_event,
				container, false);

		String day = getArguments().getString("day");

		Log.d("Event List", "Day " + day);

		dbHandler = new DBHandler(getActivity(), null, null, 0);

		List<Events> events = dbHandler.getEventByDay(day);

		String[] title = new String[events.size()];
		String[] venue = new String[events.size()];
		String[] time = new String[events.size()];
		String[] picture = new String[events.size()];
		event_id = new String[events.size()];

		for (int i = 0; i < events.size(); i++) {
			title[i] = events.get(i).get_event_name();
			venue[i] = events.get(i).get_event_vanue();
			time[i] = events.get(i).get_event_time_start() + " - "
					+ events.get(i).get_event_time_end();
			picture[i] = events.get(i).get_event_picture();
			event_id[i] = events.get(i).get_event_id();
		}

		ListView listView;

		String[] from = { "Business Mentoring for Women lala lala lala",
				"c_key", "android_key", "hacking_key" };

		ItemArrayAdapter arrayAdapter;

		listView = (ListView) rootView.findViewById(R.id.list_event);

		arrayAdapter = new ItemArrayAdapter(this.getActivity(), title, venue,
				time, picture);

		listView.setAdapter(arrayAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent myIntent = new Intent(getActivity(), EventActivity.class);
				myIntent.putExtra("EventId", event_id[position]);
				startActivity(myIntent);
			}
		});

		return rootView;
	}

	public class ItemArrayAdapter extends ArrayAdapter<String> {

		private final Activity context;
		private final String[] title;
		private final String[] venue;
		private final String[] time;
		private final String[] picture;

		public ItemArrayAdapter(Activity context, String[] title,
				String[] venue, String[] time, String[] picture) {
			super(context, R.layout.fragment_event_content, title);
			this.context = context;
			this.title = title;
			this.venue = venue;
			this.time = time;
			this.picture = picture;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.fragment_event_content,
						null);
			}

			TextView title = (TextView) rowView.findViewById(R.id.title);
			title.setText(this.title[position]);
			TextView venue = (TextView) rowView.findViewById(R.id.venue);
			venue.setText(this.venue[position]);
			TextView time = (TextView) rowView.findViewById(R.id.time);
			time.setText(this.time[position]);
			ImageView image = (ImageView) rowView.findViewById(R.id.imageView1);

			Picasso.with(context).load(picture[position]).transform(new RoundedTransformation(50,0))
					.resize(120, 120).centerCrop().into(image);

			return rowView;
		}

	}

}
