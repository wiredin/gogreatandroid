package com.motiv.gogreat.fragment;

import java.util.ArrayList;
import java.util.List;

import com.motiv.gogreat.Header;
import com.motiv.gogreat.Item;
import com.motiv.gogreat.Itinerary;
import com.motiv.gogreat.ListItem;
import com.motiv.gogreat.R;
import com.motiv.gogreat.R.layout;
import com.motiv.gogreat.subactivity.EventActivity;
import com.motiv.gogreat.TwoTextArrayAdapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ItineraryFragment extends Fragment{
	
	ListView mListView;
	
//	protected ListView getListView() {
//	    if (mListView == null) {
//	        mListView = (ListView) mListView.findViewById(R.id.lsEvent);
//	    }
//	    return mListView;
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootView = inflater.inflate(R.layout.fragment_itinerary_temp, container,
				false);
		
		
//		mListView = (ListView) rootView.findViewById(R.id.list_event);
//		
//		List<Item> items = new ArrayList<Item>();
//		items.add(new Header("17th SEPTEMBER"));
//		items.add(new ListItem("Text 1", "Rabble rabble"));
//		items.add(new ListItem("Text 2", "Rabble rabble"));
//		items.add(new ListItem("Text 3", "Rabble rabble"));
//		items.add(new ListItem("Text 4", "Rabble rabble"));
//		items.add(new Header("18th SEPTEMBER"));
//		items.add(new ListItem("Text 5", "Rabble rabble"));
//		items.add(new ListItem("Text 6", "Rabble rabble"));
//		items.add(new ListItem("Text 7", "Rabble rabble"));
//		items.add(new ListItem("Text 8", "Rabble rabble"));
//		items.add(new Header("19th SEPTEMBER"));
//		items.add(new ListItem("Text 5", "Rabble rabble"));
//		items.add(new ListItem("Text 6", "Rabble rabble"));
//		items.add(new ListItem("Text 7", "Rabble rabble"));
//		items.add(new ListItem("Text 8", "Rabble rabble"));
//		items.add(new Header("20th SEPTEMBER"));
//		items.add(new ListItem("Text 5", "Rabble rabble"));
//		items.add(new ListItem("Text 6", "Rabble rabble"));
//		items.add(new ListItem("Text 7", "Rabble rabble"));
//		items.add(new ListItem("Text 8", "Rabble rabble"));
//
//		TwoTextArrayAdapter adapter = new TwoTextArrayAdapter(getActivity(), items);
//		setListAdapter(adapter);
//		
//		mListView.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				// TODO Auto-generated method stub
//				Log.d("fared", "clicked");
//				Intent myIntent = new Intent(getActivity(),EventActivity.class);
//		        startActivity(myIntent);
//			}
//			
//			
//		});
//		
		
		
		return rootView;
	}
	
	protected void setListAdapter(ListAdapter adapter) {
	    mListView.setAdapter(adapter);
	}

	protected ListAdapter getListAdapter() {
	    ListAdapter adapter = mListView.getAdapter();
	    if (adapter instanceof HeaderViewListAdapter) {
	        return ((HeaderViewListAdapter)adapter).getWrappedAdapter();
	    } else {
	        return adapter;
	    }
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		

		
	}
	
	

}
