package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class EventDiscFragment extends Fragment {
	
	TextView title,desc,venue,time,date;
	
	DBHandler dbHandler;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootView = inflater.inflate(R.layout.fragment_event_desc, container, false);
		
		dbHandler = new DBHandler(getActivity(), null, null, 0);
		
		String eventid = getArguments().getString("EventId");
		
		Events event = dbHandler.getEventById(eventid);
		
		Log.d("fared", "description");
		
		title = (TextView) rootView.findViewById(R.id.textView4);
		desc = (TextView) rootView.findViewById(R.id.textView5);
		venue = (TextView) rootView.findViewById(R.id.venue);
		time = (TextView) rootView.findViewById(R.id.time);
		date = (TextView) rootView.findViewById(R.id.date);
		
		title.setText(event.get_event_name());
		desc.setText(event.get_event_text());
		venue.setText(event.get_event_vanue());
		time.setText(event.get_event_time_start());
		date.setText(event.get_event_date());
		
		return rootView;
	}
	
	

}
