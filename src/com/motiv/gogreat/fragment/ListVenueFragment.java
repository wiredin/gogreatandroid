package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;
import com.motiv.gogreat.subactivity.AttendeesActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ListVenueFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_list_event,
				container, false);
		ListView listView;

		Log.d("fared", "Attendess");

		String[] from = { "Hall 1", "Main Hall",
				"Hall 2", "Hall 3" };

		ArrayAdapter arrayAdapter;

		listView = (ListView) rootView.findViewById(R.id.list_event);

		arrayAdapter = new ArrayAdapter(this.getActivity(),
				R.layout.venue_content, R.id.venue, from);

		listView.setAdapter(arrayAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
//				Intent myIntent = new Intent(getActivity(),
//						AttendeesActivity.class);
//				startActivity(myIntent);
			}
		});
		return rootView;
	}

}
