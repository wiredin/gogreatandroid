package com.motiv.gogreat.fragment;

import com.motiv.gogreat.LoginActivity;
import com.motiv.gogreat.R;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Profile;
import com.motiv.gogreat.services.UpdateProfileService;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class SettingFragment extends Fragment {

	ImageView imgHeader,imgProfile;
	Button btnLogout;
	Typeface tf;

	EditText fname, lname, email, password, phone, age, compName, compPosition,
			compAddr;
	RadioButton rmale, rfemale;

	DBHandler dbHandler;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.activity_setting, container,
				false);

		dbHandler = new DBHandler(getActivity(), null, null, 0);

		tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/FARRAY.otf");

		imgHeader = (ImageView) rootView.findViewById(R.id.img_header);
		// AlphaAnimation alpha = new AlphaAnimation(.4F, .4F); // change values
		// as you want
		// alpha.setDuration(0); // Make animation instant
		// alpha.setFillAfter(true); // Tell it to persist after the animation
		// ends
		// // And then on your imageview
		// imgHeader.startAnimation(alpha);
		
		imgProfile = (ImageView) rootView.findViewById(R.id.imageView1);
		imgProfile.setImageResource(R.drawable.speaker);

		// call all fragment fields
		fname = (EditText) rootView.findViewById(R.id.edit_fname);
		lname = (EditText) rootView.findViewById(R.id.edit_lname);
		email = (EditText) rootView.findViewById(R.id.edit_email);
		password = (EditText) rootView.findViewById(R.id.edit_password);
		phone = (EditText) rootView.findViewById(R.id.edit_phone);
		age = (EditText) rootView.findViewById(R.id.edit_age);
		compName = (EditText) rootView.findViewById(R.id.editCompName);
		compPosition = (EditText) rootView.findViewById(R.id.editCompPosition);
		compAddr = (EditText) rootView.findViewById(R.id.editCompAddr);

		rmale = (RadioButton) rootView.findViewById(R.id.radio_male);
		rfemale = (RadioButton) rootView.findViewById(R.id.radio_female);

		Profile profile = dbHandler.getProfile();

		fname.setText(profile.get_first_name());
		lname.setText(profile.get_last_name());
		email.setText(profile.get_email());
		password.setText(profile.get_password());
		phone.setText(profile.get_phone_number());
		age.setText(profile.get_age());
		compName.setText(profile.get_company_name());
		compPosition.setText(profile.get_position());
		compAddr.setText(profile.get_company_address());

		String gender = profile.get_gender();

		if (gender.equals("male")) {
			rmale.setChecked(true);
			rfemale.setChecked(false);
			imgProfile.setImageResource(R.drawable.speaker);
		} else if (gender.equals("female")) {
			rfemale.setChecked(false);
			rfemale.setChecked(true);
			imgProfile.setImageResource(R.drawable.speaker_woman);
		}

		btnLogout = (Button) rootView.findViewById(R.id.btnLogout);
		btnLogout.setTypeface(tf);

		btnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dbHandler.deleteToken();
				
				Intent myintent = new Intent(getActivity(), LoginActivity.class);
				getActivity().startActivity(myintent);
				getActivity().finish();
			}

		});

		Button btnSave = (Button) rootView.findViewById(R.id.btn_save);

		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (fname.getText().toString().isEmpty()
						|| lname.getText().toString().isEmpty()) {
					showText("You need to fill in first name, last name and your email properly!");
				} else {
					Profile newprofile = new Profile();
					
					newprofile.set_first_name(fname.getText().toString());
					newprofile.set_last_name(lname.getText().toString());
					newprofile.set_email(email.getText().toString());
					newprofile.set_phone_number(phone.getText().toString());
					newprofile.set_age(age.getText().toString());
					newprofile.set_company_name(compName.getText().toString());
					newprofile.set_position(compPosition.getText().toString());
					newprofile.set_company_address(compAddr.getText().toString());
					
					if (rmale.isChecked()) {
						newprofile.set_gender("male");
					}
					if (rfemale.isChecked()) {
						newprofile.set_gender("female");
					}
					
					Log.d("Token", dbHandler.getToken());
					UpdateProfileService service = new UpdateProfileService(getActivity(), newprofile);
					service.execute();
				}
			}

		});
		
		TextView privacy = (TextView) rootView.findViewById(R.id.textPolicy);
		privacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "http://www.gogreat.my/app-privacy-policy";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
			
		});

		return rootView;
	}
	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// // TODO Auto-generated method stub
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_setting);
	//
	// tf = Typeface.createFromAsset(getAssets(), "fonts/FARRAY.otf");
	//
	// imgHeader = (ImageView) findViewById(R.id.img_header);
	// // AlphaAnimation alpha = new AlphaAnimation(.4F, .4F); // change values
	// // as you want
	// // alpha.setDuration(0); // Make animation instant
	// // alpha.setFillAfter(true); // Tell it to persist after the animation
	// // ends
	// // // And then on your imageview
	// // imgHeader.startAnimation(alpha);
	//
	// btnLogout = (Button) findViewById(R.id.btnLogout);
	// btnLogout.setTypeface(tf);
	//
	// }
	
	public void showText(String text) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("GREAT");

		// set dialog message
		alertDialogBuilder.setMessage(text).setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
