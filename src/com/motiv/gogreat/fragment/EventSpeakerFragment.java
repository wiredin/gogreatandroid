package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;
import com.motiv.gogreat.database.DBHandler;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class EventSpeakerFragment extends Fragment {

	DBHandler dbHandler;
	String[] people_id;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_list_event,
				container, false);

		ListView listView;

		Log.d("fared", "Speaker");
		
		dbHandler = new DBHandler(getActivity(), null, null, 0);

		String[] from = { "Ini Speaker 01", "Ini Speaker 02", "Ini Speaker 03",
				"Ini Speaker 04" };

		listView = (ListView) rootView.findViewById(R.id.list_event);

		ItemArrayAdapter arrayAdapter = new ItemArrayAdapter(
				this.getActivity(), from, from, from);

		listView.setAdapter(arrayAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// Intent myIntent = new
				// Intent(getActivity(),EventActivity.class);
				// startActivity(myIntent);
			}
		});

		return rootView;
	}

	public class ItemArrayAdapter extends ArrayAdapter<String> {

		private final Activity context;
		private final String[] name;
		private final String[] company;
		private final String[] position;

		public ItemArrayAdapter(Activity context, String[] name,
				String[] company, String[] position) {
			super(context, R.layout.people_content, name);
			this.context = context;
			this.name = name;
			this.company = company;
			this.position = position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.people_content, null);
			}

			TextView name = (TextView) rowView.findViewById(R.id.name);
			name.setText(this.name[position]);
			TextView company = (TextView) rowView.findViewById(R.id.company);
			company.setText(this.company[position]);
			TextView position1 = (TextView) rowView.findViewById(R.id.position);
			position1.setText(this.position[position]);
			// ImageView image = (ImageView)
			// rowView.findViewById(R.id.imageView1);

			// Picasso.with(context).load(picture[position]).transform(new
			// RoundedTransformation(50,0))
			// .resize(120, 120).centerCrop().into(image);

			return rowView;
		}

	}

}
