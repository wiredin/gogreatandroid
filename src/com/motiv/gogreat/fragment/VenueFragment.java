package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;
import com.motiv.gogreat.R.layout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class VenueFragment extends Fragment {

	RelativeLayout tab_map, tab_list;

	RelativeLayout tab1, tab2;
	TextView txtTab1, txtTab2;
	View bar1, bar2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_venue, container,
				false);
		

		bar1 = (View) rootView.findViewById(R.id.view_tab1);
		bar2 = (View) rootView.findViewById(R.id.view_tab2);

		txtTab1 = (TextView) rootView.findViewById(R.id.txt_tab1);
		txtTab2 = (TextView) rootView.findViewById(R.id.txt_tab2);

		tab_map = (RelativeLayout) rootView.findViewById(R.id.tab_map);
		tab_list = (RelativeLayout) rootView.findViewById(R.id.tab_list);

		tab_map.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				bar1.setVisibility(View.VISIBLE);
				bar2.setVisibility(View.INVISIBLE);

				txtTab1.setTextColor(getResources().getColor(R.color.theme));
				txtTab2.setTextColor(getResources().getColor(R.color.black));

				// TODO Auto-generated method stub
				Fragment newFragment = new MapVenueFragment();
				FragmentTransaction transaction = getChildFragmentManager()
						.beginTransaction();

				// Replace whatever is in the fragment_container view with this
				// fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.venue_container, newFragment);

				// Commit the transaction
				transaction.commit();
			}
		});

		tab_list.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				bar1.setVisibility(View.INVISIBLE);
				bar2.setVisibility(View.VISIBLE);

				txtTab1.setTextColor(getResources().getColor(R.color.black));
				txtTab2.setTextColor(getResources().getColor(R.color.theme));
				
				Fragment newFragment = new ListVenueFragment();
				FragmentTransaction transaction = getChildFragmentManager()
						.beginTransaction();

				// Replace whatever is in the fragment_container view with this
				// fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.venue_container, newFragment);

				// Commit the transaction
				transaction.commit();

				

			}
		});

		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();

		if (getChildFragmentManager().findFragmentById(R.id.venue_container) == null) {
			getChildFragmentManager().beginTransaction()
					.add(R.id.venue_container, new MapVenueFragment())
					.commit();
		}
	}

}
