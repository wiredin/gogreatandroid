package com.motiv.gogreat.fragment;

import com.motiv.gogreat.PlaceImageAdapter;
import com.motiv.gogreat.R;
import com.motiv.gogreat.R.layout;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

public class EventFragment extends Fragment {
	
	PlaceImageAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;

	RelativeLayout date1, date2, date3, date4;
	View fDate1, fDate2, fDate3, fDate4;
	TextView tDate1, tDate2, tDate3, tDate4;
	ImageView imgHeader;
	
	String[] time;
    String[] country;
    String[] title;
    int[] images;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.fragment_event, container,
				false);
		
		images = new int[] { R.drawable.header, R.drawable.header,
				R.drawable.header, R.drawable.header };
		
		country = new String[] { "China", "India", "United States",
	            "Indonesia" };
		
		title = new String[] { "Business", "Computer Science", "Account",
	    "Chemistry" };
		
		time = new String[] { "9.00 AM", "11.00 AM", "2.00 PM", "4.00 PM" };
		
//		imgHeader = (ImageView) rootView.findViewById(R.id.img_header);
		
		mAdapter = new PlaceImageAdapter(getActivity(), title, country, time, images);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);

		date1 = (RelativeLayout) rootView.findViewById(R.id.tab1);
		date2 = (RelativeLayout) rootView.findViewById(R.id.tab2);
		date3 = (RelativeLayout) rootView.findViewById(R.id.tab3);
		date4 = (RelativeLayout) rootView.findViewById(R.id.tab4);

		fDate1 = (View) rootView.findViewById(R.id.focus_date1);
		fDate2 = (View) rootView.findViewById(R.id.focus_date2);
		fDate3 = (View) rootView.findViewById(R.id.focus_date3);
		fDate4 = (View) rootView.findViewById(R.id.focus_date4);
		
		tDate1 = (TextView) rootView.findViewById(R.id.date1);
		tDate2 = (TextView) rootView.findViewById(R.id.date2);
		tDate3 = (TextView) rootView.findViewById(R.id.date3);
		tDate4 = (TextView) rootView.findViewById(R.id.date4);

		date1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				fDate1.setVisibility(View.VISIBLE);
				fDate2.setVisibility(View.INVISIBLE);
				fDate3.setVisibility(View.INVISIBLE);
				fDate4.setVisibility(View.INVISIBLE);
				
				tDate1.setTextColor(getResources().getColor(R.color.theme));
				tDate2.setTextColor(getResources().getColor(R.color.black));
				tDate3.setTextColor(getResources().getColor(R.color.black));
				tDate4.setTextColor(getResources().getColor(R.color.black));
				
				if (getChildFragmentManager().findFragmentById(R.id.event_container) != null) {
					
					Bundle bundle = new Bundle();
					bundle.putString("day", "1");
					
					Fragment newFragment = new ListEventFragment();
					newFragment.setArguments(bundle);
					FragmentTransaction transaction = getChildFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.event_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}

			}
		});

		date2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fDate1.setVisibility(View.INVISIBLE);
				fDate2.setVisibility(View.VISIBLE);
				fDate3.setVisibility(View.INVISIBLE);
				fDate4.setVisibility(View.INVISIBLE);
				
				tDate1.setTextColor(getResources().getColor(R.color.black));
				tDate2.setTextColor(getResources().getColor(R.color.theme));
				tDate3.setTextColor(getResources().getColor(R.color.black));
				tDate4.setTextColor(getResources().getColor(R.color.black));
				
				if (getChildFragmentManager().findFragmentById(R.id.event_container) != null) {

					Bundle bundle = new Bundle();
					bundle.putString("day", "2");
					
					Fragment newFragment = new ListEventFragment();
					newFragment.setArguments(bundle);
					FragmentTransaction transaction = getChildFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.event_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}

			}
		});

		date3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fDate1.setVisibility(View.INVISIBLE);
				fDate2.setVisibility(View.INVISIBLE);
				fDate3.setVisibility(View.VISIBLE);
				fDate4.setVisibility(View.INVISIBLE);
				
				tDate1.setTextColor(getResources().getColor(R.color.black));
				tDate2.setTextColor(getResources().getColor(R.color.black));
				tDate3.setTextColor(getResources().getColor(R.color.theme));
				tDate4.setTextColor(getResources().getColor(R.color.black));
				
				if (getChildFragmentManager().findFragmentById(R.id.event_container) != null) {

					Bundle bundle = new Bundle();
					bundle.putString("day", "3");
					
					Fragment newFragment = new ListEventFragment();
					newFragment.setArguments(bundle);
					FragmentTransaction transaction = getChildFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.event_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}
			}
		});

		date4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fDate1.setVisibility(View.INVISIBLE);
				fDate2.setVisibility(View.INVISIBLE);
				fDate3.setVisibility(View.INVISIBLE);
				fDate4.setVisibility(View.VISIBLE);
				
				tDate1.setTextColor(getResources().getColor(R.color.black));
				tDate2.setTextColor(getResources().getColor(R.color.black));
				tDate3.setTextColor(getResources().getColor(R.color.black));
				tDate4.setTextColor(getResources().getColor(R.color.theme));
				
				if (getChildFragmentManager().findFragmentById(R.id.event_container) != null) {

					Bundle bundle = new Bundle();
					bundle.putString("day", "4");
					
					Fragment newFragment = new ListEventFragment();
					newFragment.setArguments(bundle);
					FragmentTransaction transaction = getChildFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.event_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}
			}
		});

		Log.d("fared", "fragment event");

		return rootView;
	}
	
	
	
	@Override
	public void onStart() {
		super.onResume();

		if (getChildFragmentManager().findFragmentById(R.id.event_container) == null) {
			Bundle bundle = new Bundle();
			bundle.putString("day", "1");
			
			Fragment newFragment = new ListEventFragment();
			newFragment.setArguments(bundle);
			FragmentTransaction transaction = getChildFragmentManager()
					.beginTransaction();

			// Replace whatever is in the fragment_container view with
			// this fragment,
			// and add the transaction to the back stack
			transaction.replace(R.id.event_container, newFragment);

			// Commit the transaction
			transaction.commit();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mAdapter = null;
		mPager = null;
		mIndicator = null;
	}
}
