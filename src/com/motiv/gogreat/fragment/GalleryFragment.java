package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;
import com.motiv.gogreat.R.layout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GalleryFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View rootView = inflater.inflate(R.layout.fragment_gallery, container,
				false);
		
		Log.d("fared", "fragment gallery");
		return rootView;
	}
	
	

}
