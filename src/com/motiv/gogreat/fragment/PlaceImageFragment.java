package com.motiv.gogreat.fragment;

import com.motiv.gogreat.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PlaceImageFragment extends Fragment{
	
int imageResourceId;
	
	public PlaceImageFragment() {
		// TODO Auto-generated constructor stub
    	
	}

    public PlaceImageFragment(int i) {
		// TODO Auto-generated constructor stub
    	imageResourceId = i;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
//    	View rootView = inflater.inflate(R.layout.fragment_event_featured, container, false);
//		ImageView imageView = (ImageView) rootView.findViewById(R.id.img_header);
//		imageView.setImageResource(imageArray[position]);
//		
//		((ViewPager) container).addView(rootView);
//		return rootView;

        ImageView image = new ImageView(getActivity());
        image.setImageResource(imageResourceId);
        image.setScaleType(ScaleType.FIT_XY);
        
        TextView eventName = new TextView(getActivity());

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        layout.setGravity(Gravity.CENTER);
        layout.addView(image);

        return layout;
    }


}
