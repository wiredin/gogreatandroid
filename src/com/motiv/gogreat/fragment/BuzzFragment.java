package com.motiv.gogreat.fragment;

import java.util.ArrayList;
import java.util.List;

import com.motiv.gogreat.R;
import com.motiv.gogreat.RoundedTransformation;
import com.motiv.gogreat.database.Buzz;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.services.GetBuzzService;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BuzzFragment extends Fragment implements OnRefreshListener {

	SwipeRefreshLayout swipeLayout;

	RelativeLayout tab_speaker, tab_attendes;

	RelativeLayout tab1, tab2;
	TextView txtTab1, txtTab2;
	View bar1, bar2;

	DBHandler dbHandler;
	String[] people_id;
	View rootView;

	boolean error = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_list_buzz, container,
				false);

		swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);

		Log.d("fared", "Tweet");

		dbHandler = new DBHandler(getActivity(), null, null, 0);

		try {
			loadTweet();
		} catch (Exception e) {
			Log.e("Tweet", "No data - " + e);
			error = true;
			GetBuzzService service = new GetBuzzService(getActivity(), this);
			service.execute();
		}

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

		// if (getChildFragmentManager().findFragmentById(R.id.people_container)
		// == null) {
		// getChildFragmentManager().beginTransaction()
		// .add(R.id.people_container, new AttendeesFragment())
		// .commit();
		// }
	}

	public void loadTweet() {

		ListView listView;

		List<Buzz> tweets = dbHandler.getAllTweet();
		List<String> tweetName = new ArrayList<String>();

		for (int i = 0; i < tweets.size(); i++) {
			tweetName.add(tweets.get(i).get_buzz_name());
		}

		listView = (ListView) rootView.findViewById(R.id.list_event);
		// listView = new ListView(getActivity());

		ItemArrayAdapter arrayAdapter = new ItemArrayAdapter(
				this.getActivity(), tweets, tweetName);

		listView.setAdapter(arrayAdapter);
		// swipeLayout.addView(listView);

		Log.d("Buzz", "Load Tweeter");
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		swipeLayout.setRefreshing(false);
		GetBuzzService service = new GetBuzzService(getActivity(),this);
		service.execute();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		// try {
		// loadTweet();
		// } catch (Exception e) {
		// Log.e("Tweet", "No data - " + e);
		// error = true;
		// GetBuzzService service = new GetBuzzService(getActivity(), this);
		// service.execute();
		// }

		swipeLayout.setOnRefreshListener(this);

	}

	public class ItemArrayAdapter extends ArrayAdapter<String> {

		private final Activity context;
		private final List<Buzz> buzz;
		private final List<String> name;

		public ItemArrayAdapter(Activity context, List<Buzz> buzz, List<String> name) {

			super(context, R.layout.buzz_content,name);
			this.context = context;
			this.buzz = buzz;
			this.name = name;

			Log.d("Buzz", "Load ArrayAdapter");
		}

		public void add(Buzz buzz) {
			this.buzz.add(buzz);
			this.name.add(buzz.get_buzz_name());
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.buzz_content, null);
			}

			TextView name = (TextView) rowView.findViewById(R.id.name);
			name.setText(this.buzz.get(position).get_buzz_name());
			TextView screen = (TextView) rowView.findViewById(R.id.screenname);
			screen.setText(this.buzz.get(position).get_buzz_screen());
			TextView text = (TextView) rowView.findViewById(R.id.text);
			text.setText(this.buzz.get(position).get_buzz_text());
			TextView time = (TextView) rowView.findViewById(R.id.time);
			time.setText(this.buzz.get(position).get_buzz_time());
			ImageView image = (ImageView) rowView.findViewById(R.id.imageView1);

			Picasso.with(context)
					.load(this.buzz.get(position).get_buzz_picture())
					.transform(new RoundedTransformation(50, 0))
					.resize(120, 120).centerCrop().into(image);

			return rowView;
		}

	}

}
