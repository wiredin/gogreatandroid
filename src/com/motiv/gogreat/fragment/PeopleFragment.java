package com.motiv.gogreat.fragment;

import java.util.List;

import com.motiv.gogreat.R;
import com.motiv.gogreat.RoundedTransformation;
import com.motiv.gogreat.R.layout;
import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;
import com.motiv.gogreat.database.Speakers;
import com.motiv.gogreat.fragment.EventSpeakerFragment.ItemArrayAdapter;
import com.motiv.gogreat.subactivity.SpeakerActivity;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class PeopleFragment extends Fragment {

	RelativeLayout tab_speaker, tab_attendes;

	RelativeLayout tab1, tab2;
	TextView txtTab1, txtTab2;
	View bar1, bar2;

	DBHandler dbHandler;
	String[] people_id;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_list_event,
				container, false);

		ListView listView;

		Log.d("fared", "Speaker");

		dbHandler = new DBHandler(getActivity(), null, null, 0);

		List<Speakers> people = dbHandler.getAllSpeaker();

		String[] name = new String[people.size()];
		String[] company = new String[people.size()];
		String[] position = new String[people.size()];
		String[] picture = new String[people.size()];
		String[] gender = new String[people.size()];
		people_id = new String[people.size()];

		for (int i = 0; i < people.size(); i++) {
			name[i] = people.get(i).get_speak_name();
			company[i] = people.get(i).get_speak_company();
			position[i] = people.get(i).get_speak_position();
			picture[i] = people.get(i).get_speak_picture();
			gender[i] = people.get(i).get_speak_profile();
			people_id[i] = people.get(i).get_speak_id();
		}

		listView = (ListView) rootView.findViewById(R.id.list_event);

		ItemArrayAdapter arrayAdapter = new ItemArrayAdapter(
				this.getActivity(), name, company, position, gender);

		listView.setAdapter(arrayAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// Intent myIntent = new Intent(getActivity(),
				// SpeakerActivity.class);
				// startActivity(myIntent);
			}
		});

		return rootView;
	}

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// // TODO Auto-generated method stub
	//
	// View rootView = inflater.inflate(R.layout.fragment_people, container,
	// false);
	//
	// bar1 = (View) rootView.findViewById(R.id.view_tab1);
	// bar2 = (View) rootView.findViewById(R.id.view_tab2);
	//
	// txtTab1 = (TextView) rootView.findViewById(R.id.txt_tab1);
	// txtTab2 = (TextView) rootView.findViewById(R.id.txt_tab2);
	//
	// tab_speaker = (RelativeLayout) rootView.findViewById(R.id.tab_speakers);
	// tab_attendes = (RelativeLayout)
	// rootView.findViewById(R.id.tab_attendees);
	//
	// tab_speaker.setOnClickListener(new View.OnClickListener() {
	//
	//
	//
	// @Override
	// public void onClick(View v) {
	//
	// bar1.setVisibility(View.INVISIBLE);
	// bar2.setVisibility(View.VISIBLE);
	//
	// txtTab1.setTextColor(getResources().getColor(R.color.black));
	// txtTab2.setTextColor(getResources().getColor(R.color.theme));
	//
	// // TODO Auto-generated method stub
	// Fragment newFragment = new SpeakerFragment();
	// FragmentTransaction transaction =
	// getChildFragmentManager().beginTransaction();
	//
	// // Replace whatever is in the fragment_container view with this fragment,
	// // and add the transaction to the back stack
	// transaction.replace(R.id.people_container, newFragment);
	//
	// // Commit the transaction
	// transaction.commit();
	// }
	// });
	//
	// tab_attendes.setOnClickListener(new View.OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	//
	// bar1.setVisibility(View.VISIBLE);
	// bar2.setVisibility(View.INVISIBLE);
	//
	// txtTab1.setTextColor(getResources().getColor(R.color.theme));
	// txtTab2.setTextColor(getResources().getColor(R.color.black));
	//
	// Fragment newFragment = new AttendeesFragment();
	// FragmentTransaction transaction =
	// getChildFragmentManager().beginTransaction();
	//
	// // Replace whatever is in the fragment_container view with this fragment,
	// // and add the transaction to the back stack
	// transaction.replace(R.id.people_container, newFragment);
	//
	// // Commit the transaction
	// transaction.commit();
	//
	// }
	// });
	//
	// return rootView;
	// }

	@Override
	public void onResume() {
		super.onResume();

		// if (getChildFragmentManager().findFragmentById(R.id.people_container)
		// == null) {
		// getChildFragmentManager().beginTransaction()
		// .add(R.id.people_container, new AttendeesFragment())
		// .commit();
		// }
	}

	public class ItemArrayAdapter extends ArrayAdapter<String> {

		private final Activity context;
		private final String[] name;
		private final String[] company;
		private final String[] position;
		private final String[] gender;

		public ItemArrayAdapter(Activity context, String[] name,
				String[] company, String[] position, String[] gender) {
			super(context, R.layout.people_content, name);
			this.context = context;
			this.name = name;
			this.company = company;
			this.position = position;
			this.gender = gender;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.people_content, null);
			}

			TextView name = (TextView) rowView.findViewById(R.id.name);
			name.setText(this.name[position]);
			TextView company = (TextView) rowView.findViewById(R.id.company);
			company.setText(this.company[position]);
			TextView position1 = (TextView) rowView.findViewById(R.id.position);
			position1.setText(this.position[position]);
			ImageView image = (ImageView) rowView.findViewById(R.id.imageView1);
			
			if (gender[position].equals("male")) {
				image.setImageResource(R.drawable.speaker);
			} else if (gender[position].equals("female")) {
				image.setImageResource(R.drawable.speaker_woman);
			}

			// Picasso.with(context).load(picture[position]).transform(new
			// RoundedTransformation(50,0))
			// .resize(120, 120).centerCrop().into(image);

			return rowView;
		}

	}

}
