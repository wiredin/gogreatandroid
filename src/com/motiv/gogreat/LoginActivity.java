package com.motiv.gogreat;

import java.util.List;

import com.motiv.gogreat.database.DBHandler;
import com.motiv.gogreat.database.Events;
import com.motiv.gogreat.services.GetAllEventService;
import com.motiv.gogreat.services.LoginService;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends ActionBarActivity {

	TextView forgetPass, signup;
	EditText uName, password;
	Typeface tf, tf_farray;
	
	DBHandler dbHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		dbHandler = new DBHandler(this, null, null, 0);

		tf = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		tf_farray = Typeface.createFromAsset(getAssets(), "fonts/FARRAY.otf");

		password = (EditText) findViewById(R.id.password);
		uName = (EditText) findViewById(R.id.email);
		uName.setTypeface(tf);

		forgetPass = (TextView) findViewById(R.id.text_forgetpass);
		forgetPass.setTypeface(tf);
		signup = (TextView) findViewById(R.id.text_signup);

		signup.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LoginActivity.this.startActivity(new Intent(LoginActivity.this,
						RegisterActivity.class));
				finish();
			}
		});

		Button mBtn = (Button) findViewById(R.id.button1);
		mBtn.setTypeface(tf_farray);
		mBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// LoginActivity.this.startActivity(new Intent(
				// LoginActivity.this, MainActivity.class));
				// finish();
				LoginService service = new LoginService(LoginActivity.this,
						uName.getText().toString(), password.getText()
								.toString());
				
				service.execute();

			}
		});
		
		List<Events> events = dbHandler.getAllEvent();
		
		if(events == null){
			GetAllEventService service = new GetAllEventService(this);
			service.execute();
		}
		
	}

}
